
let form = document.querySelector("form");
form.addEventListener("submit", function(e) {
    e.preventDefault();
    let info = document.querySelector("textarea").value;

    axios.post('/api', {
    'info': info})
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });

});
