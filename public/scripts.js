
let form = document.querySelector("form");
function format(data) {
  let str = "<table><tr><td>Sentence</td><td>Analysis</td></tr>";
  data.forEach(function(item) {
    let strAdd = "<tr><td>" + item.sentence + "</td><td>" + JSON.stringify(item.analysis) + "</td></tr>";
    str = str + strAdd;
  });
  str = str + "</table>";
  return str;
}
form.addEventListener("submit", function(e) {
    e.preventDefault();
    let info = document.querySelector("textarea").value;

    axios.post('/api', {
    'info': info})
      .then(async function (response) {
        console.log(response);
        let y = await format(response.data.data);
        document.querySelector(".result").innerHTML = y;
      })
      .catch(function (error) {
        console.log(error);
      });

});
