const express = require('express')
const app = express()
var port = process.env.PORT || 3003;
var Sentiment = require('sentiment');
var sentiment = new Sentiment();
const cors = require('cors');
var bodyParser = require('body-parser')
app.use('/site', express.static('public'))
app.use(express.static('public'));
app.use(cors());
app.use(bodyParser.json());


async function process(str) {
    let g = str.split("\n");
    let sentences = [];
    g.forEach(function(item) {
        let r = sentiment.analyze(item);
        sentences.push({"sentence": item, "analysis": r});
    });
    return sentences;
}

app.post('/api', async function (req, res) {
    let y = await process(req.body.info);
    res.send({'data': y})
   
   // var result = sentiment.analyze('Cats are stupid.');
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`))